#!/bin/bash

arch=$1

[[ -f "generated-pipeline-$arch.yml" ]] && rm generated-pipeline-$arch.yml

touch generated-pipeline-$arch.yml
append="tee -a generated-pipeline-$arch.yml"

echo "image: docker.io/ogarcia/archlinux:devel" | $append
echo "stages: " | $append
echo "  - build" | $append

for folder in packages/*; do
    if [ -d "${folder}" ]; then

        cp job-template.yml job.yml
        packageName=${folder:9}
        echo ""
        bash -c  "./src/checkIfNeedToBeUpdated.sh $packageName $arch"

        if [ $? -eq 0 ]; then
            echo "$packageName is up to date"
            continue
        fi

        generated="y"
        echo "$packageName need to be built"

        sed -i "s/{packagename}/$packageName/" job.yml
        sed -i "s/{jobname}/$packageName-$arch/" job.yml

        echo "" | $append
        echo "" | $append
        cat job.yml | $append

        rm job.yml
    fi
done

if [ -z "$generated" ]; then
    echo "No packages need to be build, generating empty child pipeline"
    
    echo "build-null:" | $append
    echo "  stage: build" | $append
    echo "  script:" | $append
    echo "      - echo \"Nothing to see here\"" | $append
fi
