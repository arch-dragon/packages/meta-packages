#!/bin/bash

packageName=$1
arch=$2

if [ ! -f "repolist.txt" ]; then
    curl https://repo.thedragonsden.ovh/archlinux/archdragon/$arch/ > repolist.txt
fi

pkgBuild=packages/$packageName/PKGBUILD

pkgname=$packageName
pkgver=$(cat $pkgBuild | grep "pkgver=" | sed "s/'//g")
pkgrel=$(cat $pkgBuild | grep "pkgrel=" | sed "s/'//g")

fullname=$pkgname-${pkgver:7}-${pkgrel:7}

echo "$fullname"

if cat repolist.txt | grep -q "$fullname" ; then
    exit 0 # is up to date
else
    exit 1 # is not up to date
fi