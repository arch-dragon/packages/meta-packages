#!/bin/bash

echo "Finding built packages"
files=$(tree -fia ./packages | grep -e ".pkg.")

echo "${files}" | tee -a files.txt
echo "" | tee -a files.txt

mkdir meta-packages

echo "Started gathering packages"
while read package; do 
    echo "moved ${package} to packages"
    mv "${package}" meta-packages/ 
done < files.txt

rm files.txt

ls -l meta-packages