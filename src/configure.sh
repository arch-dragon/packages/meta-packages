#!/bin/bash

pacman -Sy --noconfirm --needed base-devel tree

# prepare user
groupadd users
groupadd wheel
useradd -m builder -G wheel
echo "%wheel ALL=(ALL) NOPASSWD: ALL" | tee -a /etc/sudoers
chmod 777 -R packages